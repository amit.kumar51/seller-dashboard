import express from 'express';
import sellerAuth from '../middleware/sellerAuth.js';
import { addInventory, addStore } from '../controller/dashboard.js';
import imageUpload from '../middleware/imageUpload.js';
const dashboardRouter = express.Router();

//dashboardRouter.get('/dashboard',sellerAuth, );
dashboardRouter.post('/addstore', imageUpload.single('logo'), sellerAuth, addStore);
dashboardRouter.post('/addinventory/:storeId', imageUpload.array('images', 5), sellerAuth, addInventory);

export default dashboardRouter;