import mongoose from "mongoose";
const { Schema } = mongoose;

const inventorySchema = new Schema({
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Store",
    },
    productName: {
    type: String,
    required: true,
  },

  category: {
    type: String,
    required: true,
  },

  subCategory: {
    type: String,
    required: true,
  },

  mrp: Number,
  sp: Number,
  qty: Number,
  images: [{ type: String }],

});

export default mongoose.model("Inventory", inventorySchema);
