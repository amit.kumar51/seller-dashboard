import mongoose from "mongoose";
const { Schema } = mongoose;

const storeSchema = new Schema({
  sellerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Seller",
  },
  address: {
    type: String,
    required: true,
  },

  gst: {
    type: String,
    required: true,
  },

  logo: {
    type: String,
    required: true,
  },

  storeTiming: {
    type: String,
    required: true,
  },
});

export default mongoose.model("Store", storeSchema);
