import mongoose from "mongoose";
const { Schema } = mongoose;

const sellerSchema = new Schema({
  bussinessName: {
    type: String,
    required: true,
  },

  email: {
    type: String,
    required: true,
    unique: [true, "Email should be unique"],
  },

  password: {
    type: String,
    required: true,
  }
});

export default mongoose.model("Seller", sellerSchema);
