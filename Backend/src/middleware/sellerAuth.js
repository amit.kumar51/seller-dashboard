import jwt from "jsonwebtoken";


const sellerAuth = (req, res, next) => {
  const token= req.header("auth-token");
  if (!token) res.status(401).send({ error: "Authentication token invalid" });

  try {
    const data = jwt.verify(token, process.env.SECRET_KEY);
    req.body.sellerId = data.seller.id;
    next();
  } catch (error) {
    res.status(401).send({ error: "Authentication token invalid" });
  }
};

export default sellerAuth;
