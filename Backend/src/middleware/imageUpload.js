import { v2 as cloudinary } from "cloudinary";
import { CloudinaryStorage } from "multer-storage-cloudinary";
import multer from "multer";

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET,
});

const myParams = {
  folder: "uploads",
  allowed_formats: ["jpg", "png", "jpeg"],
};

const storage = new CloudinaryStorage({
  cloudinary: cloudinary,
  params: myParams,
});

export default multer({
  limits: { fileSize: 10 * 1024 * 1024 },
  storage: storage,
});
