
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import Seller from "../models/sellerModel.js";

//Register User Controller
export const register = async (req, res) => {
  try {
    const {bussinessName, email, password} = req.body;
    let seller = await Seller.findOne({ email });

    if(seller) throw new Error("User with this email already exists");

    const salt = bcrypt.genSaltSync(10);
    const securePass = await bcrypt.hashSync(password, salt);

    seller = await Seller.create({
        bussinessName,
        email,
        password: securePass
      });
  
    res.status(200).send(seller);
  } catch (error) {
    res.status(500).send({ error: error.message || "Internal Server Error" });
  }
};

// Login Controller
export const login = async (req, res) => {
  const { email, password } = req.body;
    
  try {
    const seller = await Seller.findOne({ email });
    
    if (!seller) throw new Error("Please enter login with correct credentials");
    
    const passwordCompare = await bcrypt.compare(password, seller.password);
    
    if (!passwordCompare)
      return res
        .status(400)
        .json({ error: "Please try to login with correct credentials" });

    const data = {
      seller: {
        id: seller.id,
      },
    };

    const authtoken = jwt.sign(data, process.env.SECRET_KEY);
    res.json({ seller, authtoken });
  } catch (error) {
    return res.status(404).send({
      error: true,
      message: error.message || "Internal server error",
    });
  }
};
