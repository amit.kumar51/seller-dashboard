import inventoryModel from "../models/inventoryModel.js";
import Inventory from "../models/inventoryModel.js";
import Store from "../models/storeModel.js";


export const addStore = async (req, res) => {
    try {
        const {address, gst, storeTiming, sellerId} = req.body;
        const logo = req.file?.path
        const store = await Store.create({
          address,
          logo,
          gst,
          storeTiming,
          sellerId
        });

        res.status(200).send(store);

    } catch (error) {
        res.status(500).send({error: error?.message});
    }
}

export const search = async(req, res) => {
    try {
        const {query} = req.query;

        const products = await Inventory.find({
            'productName': { $regex: query, $options: 'i' },
          });
      
          res.status(200).json({ products });
        
    } catch (error) {
        
    }
}

export const addInventory = async(req, res) => {
    try {
        const [productName, category, subCategory, mrp, sp, qty] = req.body;
        const sellerId = (req.params)
        const images = req.files.map(file => file.path);
        const inventory = await Inventory.create({
            productName,
            category,
            subCategory,
            mrp,
            sp,
            qty,
            images,
            sellerId
        })

        res.status(200).send(inventory);
    } catch (error) {
        res.status(500).send({error: error?.message});
    }
}