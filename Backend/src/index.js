import express from "express";
import dotenv from "dotenv";
import dbRun from "./config/db.js";
import authRouter from "./routes/authRoutes.js";
import dashboardRouter from "./routes/dashboardRoutes.js";

const app = express();
dotenv.config();

const port = process.env.PORT || 5000;

app.use(express.json());

//Routes
app.use('/auth/api', authRouter);
app.use('/dashboard/api', dashboardRouter);

dbRun();

app.listen(port, () => {
    console.log(`Server is running ${port}`);
})
